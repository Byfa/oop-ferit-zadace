#include "Season_header.h"

int main()
{
	std::string fileName("shows.tv");
	const int COUNT = 10;
	Episode** episodes = loadEpisodesFromFile(fileName, COUNT);

	const Season* season = new Season(episodes, COUNT);
	Season* seasonCopy = new Season(*season);

	for (int i = 0; i < COUNT; i++) {
		delete episodes[i];
	}
	delete[] episodes;

	const Episode episode = (*season)[0];
	const Episode episodeCopy = (*seasonCopy)[0];

	std::cout << episode << std::endl;
	std::cout << episodeCopy << std::endl;

	if (episodeCopy == episode) {
		std::cout << "HW points++" << std::endl;
	}
	if (&seasonCopy[0] == &season[0]) {
		std::cout << "HW points--" << std::endl;
	}

	std::cout << "Report:" << "\n\t"
		<< "Avg rating: " << season->getAverageRating() << "\n\t"
		<< "Total viewers: " << season->getTotalViews() << "\n\t"
		<< "Best episode: " << season->getBestEpisode() << "\n\t"
		<< std::endl;

	delete season;
	delete seasonCopy;	
	
	return 0;
}

//## Primjer izlaza
//10, 70.18,9.85,11,45,Pilot
//10, 70.18,9.85,11,45,Pilot
//HW points++
//Report:
//	Avg rating: 7.3782
//    Total viewers: 100
//    Best episode: 10, 79.33,9.89,12,45,Paternity



#include "Season_header.h"

Episode Season::operator[](int index) {
	Episode* ep = *mEpisodes;
	return ep[index];
}

const Episode Season::operator[](int index)const {
	Episode* ep = *mEpisodes;
	return ep[index];
}

float Season::getAverageRating()const {
	float sum = 0, avg;
	int i;
	for(i=0; i<this->mCount; i++)
		sum+=mEpisodes[i]->getAverageScore();
	avg = sum/mCount;
	return avg;
}

int Season::getTotalViews()const {
	int sum = 0, i;
	for (i = 0; i < mCount; i++)
		sum += mEpisodes[i]->getViewerCount();
	return sum;
}

Episode Season::getBestEpisode()const {
	float max = mEpisodes[0]->getMaxScore();
	int index = 0;
	for(int i=1; i<mCount; i++)
		if (mEpisodes[i]->getMaxScore() > max) {
			max = mEpisodes[i]->getMaxScore();
			index = i;
		}
	return *mEpisodes[index];
}

Episode** Season::getmEpisodes() const {
	return mEpisodes;
}

int Season::getmCount() const {
	return mCount;
}

Episode* Season::operator=(const Season* season) {
	return *season->mEpisodes;
}

#include "Episode_header.h"

class Season{
	private:
		Episode** mEpisodes;
		int mCount;
			
	public:
		Season() : mEpisodes(NULL), mCount(0){}
		Season(Episode** Episodes, const int Count) : mCount(Count){
			mEpisodes = new Episode*[Count];
			for(int i = 0; i < mCount; i++){
				mEpisodes[i] = new Episode(*Episodes[i]);			
			}
		}
		float getAverageRating()const;
		int getTotalViews()const;
		Episode getBestEpisode()const;
		Episode operator[](int); 
		const Episode operator[](int) const; 
		Season(const Season& season) : mCount(season.mCount){
			mEpisodes = new Episode*[season.mCount];
			int i;
			for(i = 0; i < mCount; i++){
				mEpisodes[i] = season.mEpisodes[i];
			}
		}
		~Season(){
			int i;
			for(i = 0; i < this->mCount; i++){
				delete this->mEpisodes[i];
			}
			delete[] this->mEpisodes;
		}
		
		Episode** getmEpisodes() const;
		int getmCount() const;
		Episode* operator=(const Season*);
}; 







#pragma once
#include "Episode_header.h"
#include <stdexcept>



class Season : EpisodeNotFoundException {
	friend class ConsolePrinter;
	friend class FilePrinter;

	friend std::ostream& operator << (std::ostream& out, const Season& c);
	friend bool operator==(const Season& ref1, const Season& ref2);

private:
	std::vector<Episode> mEpisodes;
	//int mCount;

public:
	Season() : mEpisodes(std::vector<Episode>()) {}
	//Season(Episode**, int)
	Season(const Season* season) : mEpisodes(std::vector<Episode>(season->mEpisodes)) {}
	Season(const std::vector<Episode>& ep) : mEpisodes(ep) {}
	~Season() {}

	float getAverageRating()const;
	int getTotalViews()const;
	Episode getBestEpisode()const;
	//std::vector<Episode> getmEpisodes();
	//std::vector<Episode> getSize();

	//Episode* operator=(const Season*);
	//Episode operator[](int);
	const Episode operator[](unsigned int) const;

	void add(const Episode& ref);
	void remove(const std::string name);
};


#include "Description_header.h"


int Description::getmNumber_Ep()const {
	return mNumber_Ep;
}

float Description::getmDuration()const
{
	return mDuration;
}

std::string Description::getmName()const {
	return mName;
}
void Description::SetmNumber_Ep(int Number_Ep) {
	mNumber_Ep = Number_Ep;
}
void Description::SetmDuration(float Duration) {
	mDuration = Duration;
}
void Description::SetmName(std::string Name) {
	mName = Name;
}

std::ostream& operator<<(std::ostream& out, const Description& mDescription) {
	return	out << mDescription.mNumber_Ep << ", " << mDescription.mDuration << ", " << mDescription.mName << std::endl;
}

std::istream& operator>>(std::istream& input, Description& description) {
	char buffer;
	input >> description.mNumber_Ep >> buffer >> description.mDuration >> buffer;
	std::getline(input, description.mName);
	return input;
}

bool operator==(const Description& description1, const Description& description2) {
	if (description1.getmNumber_Ep() == description2.getmNumber_Ep() && description1.getmDuration() == description2.getmDuration() && description1.getmName() == description2.getmName())
		return true;
	return false;
}

Description& Description::operator=(const Description& ref) {
	mNumber_Ep = ref.getmNumber_Ep();
	mDuration = ref.getmDuration();
	mName = ref.getmName();
	return *this;
}
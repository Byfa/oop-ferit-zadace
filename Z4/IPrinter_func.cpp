#include"IPrinter_header.h"

void ConsolePrinter::print(Season& season) 
{ 
	for (unsigned int i = 0; i < season.mEpisodes.size(); i++)
	{
		std::cout << season.mEpisodes.at(i);
	} 
	std::cout << std::endl;
} 


void FilePrinter::print(Season& season) 
{ 
	std::fstream output(FileName, std::ios::app);
	if (!output.is_open())exit(0);
	for (unsigned int i = 0; i < season.mEpisodes.size(); i++)
	{ 
		output << season.mEpisodes.at(i);
	} 
	output << std::endl;
	output.close();
}

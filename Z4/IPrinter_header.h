#include"Season_header.h"

//class IPrinter {
//public:
//	virtual ~IPrinter() = 0;
//	virtual void print(const Season& ref)const = 0;
//};
//
//class ConsolePrinter : public IPrinter {
//
//public:
//	ConsolePrinter();
//	void print(const Season& ref)const;
//};
//
//class FilePrinter : public IPrinter {
//
//public:
//	FilePrinter();
//	void print(const Season& ref)const;
//};


class IPrinter {
public:
	virtual void print(Season& season) = 0;
};

class ConsolePrinter : public IPrinter, public Season {
private:
	Season season;
public:
	ConsolePrinter() {};
	void print(Season&);
};


class FilePrinter : public IPrinter {
private:
	Season season;
	std::string FileName;
	std::ofstream out;
public:
	FilePrinter(std::string name) : FileName(name) {}
	void print(Season&);
	~FilePrinter() 
	{
		this->out.close();
	}
};

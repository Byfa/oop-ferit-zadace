#include "Episode_header.h"


float generateRandomScore() {
	return (0.0 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (10.0 - 0.0))));
}

void Episode::addView(float score) {
	if (score > mMaxScore) mMaxScore = score;
	mMaxScore += score;
	mViewers++;
}

float Episode::getMaxScore()const {
	return mMaxScore;
}

float Episode::getAverageScore()const {
	return mScoreSum / mViewers;
}

int Episode::getViewerCount()const {
	return mViewers;
}

float Episode::getmScoreSum()const {
	return mScoreSum;
}

void Episode::SetmViewers(int Viewers) {
	mViewers = Viewers;
}
void Episode::SetmScoreSum(float ScoreSum) {
	mScoreSum = ScoreSum;
}

void Episode::SetmMaxScore(float MaxScore) {
	mMaxScore = MaxScore;
}

Description& Episode::getObj() {
	return mDescription;
}

void print(Episode** episodes, const int COUNT) {
	for (int i = 0; i < COUNT; i++) {
		std::cout << i << ". " << *episodes[i];
	}
}

void swap(Episode** ref1, Episode** ref2)
{
	Episode temp = **ref1;
	**ref1 = **ref2;
	**ref2 = temp;
}

bool operator<(const Episode& ep1, const Episode& ep2) {
	return ep1.mDescription.getmName() < ep2.mDescription.getmName();
}

bool operator==(const Episode& ep1, const Episode& ep2) {
	if (ep1.mViewers == ep2.mViewers && ep1.mScoreSum == ep2.mScoreSum && ep1.mMaxScore == ep2.mMaxScore && ep1.mDescription == ep2.mDescription)
		return true;
	return false;
}

//void SetObj(int Number_Ep, float Duration, std::string Name) {
//	Description desc = Description(Number_Ep, Duration, Name);
//}

void sort(Episode** ep, const int count)
{
	for (int i = 0; i < count; i++) {
		for (int j = i + 1; j < count; j++) {
			if (*ep[j] < *ep[i]) {
				swap(&ep[j], &ep[i]);
			}
		}
	}
}

std::ostream& operator<<(std::ostream& out, const Episode& ep) {
	return	out << ep.mViewers << ", " << ep.mScoreSum << ". " << ep.mMaxScore << ", " << ep.mDescription;
}

std::istream& operator>>(std::istream& input, Episode& ep) {
	char buffer;
	input >> buffer >> buffer >> ep.mViewers >> buffer >> ep.mScoreSum >> buffer >> ep.mMaxScore >> buffer >> ep.mDescription;
	return input;
}

void persistToFile(std::string name, std::vector<Episode> ep) {
	std::ofstream OutputFile(name.c_str());
	if (!OutputFile)exit(0);
	for (unsigned int i = 0; i < ep.size(); i++)
		OutputFile << ep[i];
	OutputFile.close();
}

std::vector<Episode> loadEpisodesFromFile(std::string fileName) {
	std::ifstream InputFile(fileName.c_str());
	if (!InputFile)exit(0);
	std::vector<Episode> episodes;
	Episode ep;
	//for (unsigned int i = 0; i < ep.size(); i++) {
	//	//ep[i] = new Episode;
	//	InputFile >> ep[i];
	//}
	while (!InputFile.eof()) {
		InputFile >> ep;
		episodes.push_back(ep);
	}
	InputFile.close();
	return episodes;
}


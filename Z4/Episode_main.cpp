#include "IPrinter_header.h"

int main()
{
	IPrinter* printer = new ConsolePrinter();
	// IPrinter* printer = new FilePrinter("name.txt");

	std::string fileName("shows.tv");
	std::vector<Episode> episodes = loadEpisodesFromFile(fileName);
	Season* season = new Season(episodes);

	printer->print(*season);
	season->add(Episode(10, 84.56, 9.88, Description(11, 45, "Christmas special")));
	printer->print(*season);
	try {
		season->remove("Pilot");
		season->remove("Nope.");
	}
	catch (const EpisodeNotFoundException& ex) {
		std::cout << ex.what() << ", Name: " << ex.getName() << "\n\n";
	}
	printer->print(*season);

	delete printer;
	delete season;

	system("PAUSE");

	return 0;
}

//## Primjer ulazne datoteke
//10,70.18,9.85,11,45,Pilot
//10,79.33,9.89,12,45,Paternity
//10,78.88,8.77,13,45,Occam's Razor
//10,69.69,9.38,14,45,Maternity
//10,77.95,9.82,15,45,Damned If You Do
//10,75.88,9.33,16,45,The Socratic Method
//10,74.91,9.46,17,45,Fidelity
//10,69.44,9.78,18,45,Poison
//10,72.34,8.54,19,45,DNR
//10,69.22,9.47,110,45,Histories
//
//
//## Primjer izlaza
//
//
//******************************
//10, 70.18,9.85,11,45,Pilot
//10, 79.33,9.89,12,45,Paternity
//10, 78.88,8.77,13,45,Occam's Razor
//10, 69.69,9.38,14,45,Maternity
//10, 77.95,9.82,15,45,Damned If You Do
//10, 75.88,9.33,16,45,The Socratic Method
//10, 74.91,9.46,17,45,Fidelity
//10, 69.44,9.78,18,45,Poison
//10, 72.34,8.54,19,45,DNR
//10, 69.22,9.47,110,45,Histories
//******************************
//
//******************************
//10, 70.18,9.85,11,45,Pilot
//10, 79.33,9.89,12,45,Paternity
//10, 78.88,8.77,13,45,Occam's Razor
//10, 69.69,9.38,14,45,Maternity
//10, 77.95,9.82,15,45,Damned If You Do
//10, 75.88,9.33,16,45,The Socratic Method
//10, 74.91,9.46,17,45,Fidelity
//10, 69.44,9.78,18,45,Poison
//10, 72.34,8.54,19,45,DNR
//10, 69.22,9.47,110,45,Histories
//10, 84.56,9.88,11,45,Christmas special
//******************************
//
//No such episode found., Name: Nope.
//
//******************************
//10, 79.33,9.89,12,45,Paternity
//10, 78.88,8.77,13,45,Occam's Razor
//10, 69.69,9.38,14,45,Maternity
//10, 77.95,9.82,15,45,Damned If You Do
//10, 75.88,9.33,16,45,The Socratic Method
//10, 74.91,9.46,17,45,Fidelity
//10, 69.44,9.78,18,45,Poison
//10, 72.34,8.54,19,45,DNR
//10, 69.22,9.47,110,45,Histories
//10, 84.56,9.88,11,45,Christmas special
//******************************


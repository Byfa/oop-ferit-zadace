#pragma once

#include <string> 
#include <vector> 
#include <stdexcept>

class EpisodeNotFoundException : public std::runtime_error {

	std::string name;
	std::string msg;

public:
	//EpisodeNotFoundException() : std::runtime_error("Error") {}
	//EpisodeNotFoundException(std::string msg) : std::runtime_error(msg) {}
	//std::string getName()const;
	//void setName(std::string str);
	
	EpisodeNotFoundException() throw() : std::runtime_error("No message") {}
	EpisodeNotFoundException(std::string txt, std::string Name) throw() :msg(txt), name(Name), std::runtime_error(txt) {} 
	virtual const char* what() const throw() 
	{ 
		return msg.c_str(); 
	} 
	std::string getName() const {
		return name; 
	}
};
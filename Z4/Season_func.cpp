#include "Season_header.h"

//Episode Season::operator[](int index) {
//	Episode* ep = *mEpisodes;
//	return ep[index];
//}

const Episode Season::operator[](unsigned int index)const {
	if (index > mEpisodes.size()) 
		throw std::runtime_error("Previliki index");
	return mEpisodes[index];
}

float Season::getAverageRating()const {
	float sum = 0, avg;
	unsigned int i;
	for (i = 0; i < this->mEpisodes.size(); i++)
		sum += mEpisodes[i].getAverageScore();
	avg = sum / mEpisodes.size();
	return avg;
}

int Season::getTotalViews()const {
	int sum = 0;
	unsigned int i;
	for (i = 0; i < mEpisodes.size(); i++)
		sum += mEpisodes[i].getViewerCount();
	return sum;
}

Episode Season::getBestEpisode()const {
	float max = mEpisodes[0].getMaxScore();
	int index = 0;
	for (unsigned int i = 1; i < mEpisodes.size(); i++)
		if (mEpisodes[i].getMaxScore() > max) {
			max = mEpisodes[i].getMaxScore();
			index = i;
		}
	return mEpisodes[index];
}

//Episode** Season::getmEpisodes() const {
//	return mEpisodes;
//}
//
//Episode* Season::operator=(const Season* season) {
//	return *season->mEpisodes;
//}
//std::vector<Episode> Season::getSize() const {
//	return mEpisodes.size();
//}

bool operator==(const Season& ref1, const Season& ref2)
{
	bool test = false;
	if (ref1.mEpisodes.size() == ref2.mEpisodes.size()) {
		for (unsigned int i = 0; i < ref1.mEpisodes.size(); i++) {
			if (ref1.mEpisodes != ref2.mEpisodes) {
			}
		}
		test = true;
	}
	return test;
}

std::ostream& operator << (std::ostream& out, const Season& ref) {
	for (unsigned int i = 0; i < ref.mEpisodes.size(); i++) {
		out << ref.mEpisodes.at(i) << std::endl;	//???
	}
	return out;
}


void Season::add(const Episode& ref)
{
	this->mEpisodes.push_back(ref);
}

void Season::remove(const std::string removeName)
{
	std::string name; int rm = -1;
	for (unsigned int i = 0; i < this->mEpisodes.size(); i++)
	{ 
		name = this->mEpisodes.at(i).getObj().getmName();
		if (name == removeName)
		{ 
			rm = i; 
		} 
	} 
	if (rm == -1) 
	{
		throw EpisodeNotFoundException("No such episode found.", removeName);
	}
	else 
	{ 
		this->mEpisodes.erase(mEpisodes.begin() + rm, mEpisodes.begin() + rm+1); 
	}

}

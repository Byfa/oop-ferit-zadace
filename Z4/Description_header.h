#pragma once
#include<iostream>
#include<cstdlib>
#include<ctime>
#include<ctype.h>
#include<fstream>
#include<string>

class Description {
	friend std::ostream& operator<<(std::ostream&, const Description&);
	friend std::istream& operator>>(std::istream&, Description&);
	friend bool operator==(const Description&, const Description&);
private:
	int mNumber_Ep;
	float mDuration;
	std::string mName;

public:
	Description() :mNumber_Ep(0), mDuration(0), mName("x") {}
	Description(int Number_Ep, float Duration, std::string Name) :mNumber_Ep(Number_Ep), mDuration(Duration), mName(Name) {}
	int getmNumber_Ep()const;
	float getmDuration()const;
	std::string getmName()const;
	void SetmNumber_Ep(int);
	void SetmDuration(float);
	void SetmName(std::string);
	Description& operator=(const Description&);
};


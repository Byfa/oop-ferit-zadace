#include<iostream>
#include<cstdlib>
#include<ctime>
#include<ctype.h>
#include<fstream>
#include<string>
#include<vector>
#include<iterator>

#include "Description_header.h"
#include "EpisodeNotFoundException.h"

class Episode {
	friend std::ostream& operator<<(std::ostream&, const Episode&);
	friend std::istream& operator>>(std::istream&, Episode&);
	friend bool operator<(const Episode&, const Episode&);
	friend bool operator==(const Episode&, const Episode&);
	friend void print(Episode**, const int);
	friend void sort(Episode**, const int);
	//friend void persistToFile(std::string, Episode**, const int);
	friend void persistToFile(std::string name, std::vector<Episode> ep);

private:

	int mViewers;
	float mScoreSum;
	float mMaxScore;
	Description mDescription;

public:

	Episode() :mViewers(0), mScoreSum(0), mMaxScore(0), mDescription() {}
	Episode(int Viewers, float ScoreSum, float MaxScore, Description description) :mViewers(Viewers), mScoreSum(ScoreSum), mMaxScore(MaxScore), mDescription(description) {}
	void addView(float);
	float getMaxScore()const;
	float getAverageScore()const;
	int getViewerCount()const;
	float getmScoreSum()const;
	Description& getObj();
	void SetmViewers(int);
	void SetmScoreSum(float);
	void SetmMaxScore(float);
	//void SetObj(int Number_Ep, float Duration, std::string Name);
	//Episode& operator=(Episode&);
};

float generateRandomScore();
void swap(Episode** ref1, Episode** ref2);
//Episode** loadEpisodesFromFile(std::string, const int);
std::vector<Episode> loadEpisodesFromFile(std::string);










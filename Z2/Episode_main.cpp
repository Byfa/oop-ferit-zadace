//njihove metode, kao i globalne funkcije kako bi se testni program u nastavku mogao ispravno izvesti. 
//Kod sortiranja i ispisa koristiti odgovarajuĉi preoptereĉeni operator. Sortirati uzlazno prema prvom slovu imena epizode.

#include "Episode_header.h"

int main()
{
//	srand (static_cast <unsigned> (time(0)));
//	
//	Episode *ep1, *ep2;
//	ep1 = new Episode();
//	ep2 = new Episode(10, 64.39, 8.7);
//	int viewers = 10;
//	for (int i = 0; i < viewers; i++) {
//		ep1->addView(generateRandomScore());
//		std::cout << ep1->getMaxScore() << std::endl;
//	}
//	if (ep1->getAverageScore() > ep2->getAverageScore()) {
//		std::cout << "Viewers: " << ep1->getViewerCount() << std::endl;
//	}
//	else {
//		std::cout << "Viewers: " << ep2->getViewerCount() << std::endl;
//	}
//	delete ep1;
//	delete ep2;
//	
	Description description(1, 45, "Pilot");
	std::cout << description << std::endl;
	Episode episode(10, 88.64, 9.78, description);
	std::cout << episode << std::endl;

	// Assume that the number of rows in the text file is always at least 10. 
	// Assume a valid input file.
	std::string fileName("shows.tv");
	const int COUNT = 10;

	std::ifstream input(fileName.c_str());	
	Episode* episodes[COUNT];

	if (input.is_open() == false)
		return 1;

	for (int i = 0; i < COUNT; i++) {
		episodes[i] = new Episode();
		input >> *episodes[i];
	}	
	input.close();

	std::cout << "Episodes:" << std::endl;
	print(episodes, COUNT);	
	sort(episodes, COUNT);
	std::cout << "Sorted episodes:" << std::endl;
	print(episodes, COUNT);

	persistToFile("sorted.tv", episodes, COUNT);

	for (int i = 0; i < COUNT; i++) {
		delete episodes[i];
	}
	
	return 0;
}

//## Primjer izlaza
//	1,45,Pilot
//	10, 88.64,9.78,1,45,Pilot
//	Episodes:
//	0. 10, 70.18,9.85,11,45,Pilot
//	1. 10, 79.33,9.89,12,45,Paternity
//	2. 10, 78.88,8.77,13,45,Occam's Razor
//	3. 10, 69.69,9.38,14,45,Maternity
//	4. 10, 77.95,9.82,15,45,Damned If You Do
//	5. 10, 75.88,9.33,16,45,The Socratic Method
//	6. 10, 74.91,9.46,17,45,Fidelity
//	7. 10, 69.44,9.78,18,45,Poison
//	8. 10, 72.34,8.54,19,45,DNR
//	9. 10, 69.22,9.47,110,45,Histories
//	Sorted episodes:
//	0. 10, 77.95,9.82,15,45,Damned If You Do
//	1. 10, 72.34,8.54,19,45,DNR
//	2. 10, 74.91,9.46,17,45,Fidelity
//	3. 10, 69.22,9.47,110,45,Histories
//	4. 10, 69.69,9.38,14,45,Maternity
//	5. 10, 78.88,8.77,13,45,Occam's Razor
//	6. 10, 70.18,9.85,11,45,Pilot
//	7. 10, 79.33,9.89,12,45,Paternity
//	8. 10, 69.44,9.78,18,45,Poison
//	9. 10, 75.88,9.33,16,45,The Socratic Method



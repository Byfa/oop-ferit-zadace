#include<iostream>
#include<cstdlib>
#include<ctime>
#include<ctype.h>
#include<fstream>
#include<string>
#include "Description_header.h"

class Episode{
	
	friend class Description;
	friend std::ostream& operator<<(std::ostream&,const Episode&);
	friend std::istream& operator>>(std::istream&, Episode&);
	friend void print(Episode**, const int);
	friend void sort(Episode**, const int);
	friend void persistToFile(std::string, Episode**, const int);
	
	private:
		
		int mViewers;
		float mScoreSum;
		float mMaxScore;
		Description mDescription;
		
	public:

		Episode():mViewers(0), mScoreSum(0), mMaxScore(0), mDescription(){}
		Episode(int Viewers, float ScoreSum, float MaxScore, Description description):mViewers(Viewers), mScoreSum(ScoreSum), mMaxScore(MaxScore), mDescription(description){}
		int addView(float );
		float getMaxScore()const;
		float getAverageScore()const; 
		int getViewerCount()const; 
		float getmScoreSum()const;
		Description& getObj();
		void SetmViewers(int);
		void SetmScoreSum(float);
		void SetmMaxScore(float); 
};

float generateRandomScore();
void swap(int*, int*);


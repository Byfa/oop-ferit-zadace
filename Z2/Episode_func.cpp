#include "Episode_header.h"


float generateRandomScore(){
	return (0.0 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(10.0 - 0.0))));
}

int Episode::addView(float score){
	if(score > mMaxScore) mMaxScore=score;
	mMaxScore+=score;
	mViewers++;
}

float Episode::getMaxScore()const{
	return mMaxScore;	
}

float Episode::getAverageScore()const{
	return mScoreSum/mViewers;
}

int Episode::getViewerCount()const{
	return mViewers;
}

float Episode::getmScoreSum()const{
	return mScoreSum;
}

void Episode::SetmViewers(int Viewers){
	mViewers=Viewers;
}
void Episode::SetmScoreSum(float ScoreSum){
	mScoreSum = ScoreSum;
}

void Episode::SetmMaxScore(float MaxScore){
	mMaxScore = MaxScore;
}

Description& Episode::getObj(){
	return mDescription;
}

void print(Episode** episodes, const int COUNT){
	for (int i = 0; i < COUNT; i++) {
		std::cout<<i<<". "<<*episodes[i];
	}
}

void swap(Episode** ref1, Episode** ref2) 
{ 
    Episode temp = **ref1; 
    **ref1 = **ref2; 
    **ref2 = temp; 
} 


void sort(Episode** ep, const int count)
{
	int i,j,k,n=0,cnt=0;
	char temp;
	for( i=0; i < count; i++){
		for(j=0; j<(count-i-1); j++)
		{	
		
			std::string title1 = ep[j]->getObj().getmName();
			std::string title2 = ep[j+1]->getObj().getmName();
			
			if(title2 < title1)
			{
				swap(&ep[j], &ep[j+1]);
			}
			
			if(title2[0] == title1[0])
			{
				
			}
					
		}
		for(j=0; j<(count-1-i); j++)
		{	
			std::string title1 = ep[j]->getObj().getmName();
			std::string title2 = ep[j+1]->getObj().getmName();
		
			if(title2[0] == title1[0])
			{
				n=0;
				while(title1[n])
				{
					temp = title1[n];
					title1[n] = tolower(temp);
					n++;
				}
				while(title2[n])
				{
					temp = title2[n];
					title2[n] = tolower(temp);
					n++;
				}
				if(title1.length() < title2.length())
				{
					for(k=0; k < title1.length(); k++)
					{	
						if(title2[k] < title1[k])
						{
							swap(&ep[j], &ep[j+1]);
						}
					}
				}
				else{
					for(k=0; k < title2.length(); k++)
					{	
						if(title2[k] < title1[k])
						{
							swap(&ep[j], &ep[j+1]);
						}
					}
				}
			}
		}
	
	}
}

std::ostream& operator<<(std::ostream& out, const Episode& ep){
	return	out<<ep.mViewers<<", "<<ep.mScoreSum<<". "<<ep.mMaxScore<<", "<<ep.mDescription;
}

std::istream& operator>>(std::istream& input, Episode& ep){
	char buffer;
	input>>buffer>>buffer>>ep.mViewers>>buffer>>ep.mScoreSum>>buffer>>ep.mMaxScore>>buffer>>ep.mDescription;
	return input;
}

void persistToFile(std::string name, Episode** ep, const int count){
	std::ofstream OutputFile(name.c_str());
	for (int i = 0; i < count; i++) {
		OutputFile<<*ep[i];
		//OutputFile<<i<<". "<<count<<", "<<ep[i]->getmScoreSum()<<", "<<ep[i]->getMaxScore()<<", "<<ep[i]->getObj().getmNumber_Ep()<<", "<<ep[i]->getObj().getmDuration()<<", "<<ep[i]->getObj().getmName()<<std::endl;
	}
	OutputFile.close();
}


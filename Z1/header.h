#include<iostream>
#include<cstdlib>
#include<ctime>

class Episode{
	
	private:
		
		int viewers;
		float sumaOcjena;
		float maxOcjena;
		
	public:
		
		Episode();
		Episode(int viewer, float suma, float max);
		//float generateRandomScore(); 
		int addView(float ocjena);
		float getMaxScore()const;
		float getAverageScore()const; 
		int getViewerCount()const; 
};

float generateRandomScore();

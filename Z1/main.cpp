#include "header.h"

int main()
{
	srand (static_cast <unsigned> (time(0)));
	
	Episode *ep1, *ep2;
	ep1 = new Episode();
	ep2 = new Episode(10, 64.39, 8.7);
	int viewers = 10;
	for (int i = 0; i < viewers; i++) {
		ep1->addView(generateRandomScore());
		std::cout << ep1->getMaxScore() << std::endl;
	}
	if (ep1->getAverageScore() > ep2->getAverageScore()) {
		std::cout << "Viewers: " << ep1->getViewerCount() << std::endl;
	}
	else {
		std::cout << "Viewers: " << ep2->getViewerCount() << std::endl;
	}
	delete ep1;
	delete ep2;
	
	return 0;
}

// Primjer izlaza

//1.	0.0125126
//2.	5.63585
//3.	5.63585
//4.	8.08741
//5.	8.08741
//6.	8.08741
//7.	8.08741
//8.	8.95962
//9.	8.95962
//10.	8.95962
//11.	Viewers: 10 


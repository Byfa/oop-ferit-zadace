#include "header.h"

Episode::Episode(){
	viewers = 0;
	sumaOcjena = 0;
	maxOcjena = 0;
}

Episode::Episode(int viewer, float suma, float max){
	viewers = viewer;
	sumaOcjena = suma;
	maxOcjena = max;
}

//float Episode::generateRandomScore()
float generateRandomScore(){
	return (0.0 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(10.0 - 0.0))));
}

int Episode::addView(float ocjena){
	if(ocjena > maxOcjena) maxOcjena=ocjena;
	sumaOcjena+=ocjena;
	viewers++;
}

float Episode::getMaxScore()const{
	return maxOcjena;	
}

float Episode::getAverageScore()const{
	return sumaOcjena/viewers;
}

int Episode::getViewerCount()const{
	return viewers;
}
